package main.java

import org.apache.spark.sql.{Column, DataFrame, Dataset, Row, SparkSession}
import org.apache.spark.sql.functions.{array, lit, udf}
import org.apache.spark.sql.types.{DoubleType, NumericType}

/**
  * Created by dan on 11.03.2017.
  *
  *
  * NOTE: !!!!! THE DECISIONS WILL HAVE AN EXTRA COLUMN IN THEM, I.E. THE 'INCR' COLUMN --> IF NEED BE, REMOVE IT
  */
object Phase1_GreedyAndBackfit {

  // Will store a map between field names and the indexes in the row
  /*val fieldIndexMap : Map[String, Integer] = Map[String, Integer](
    "BAG_ID" -> 0,
    "FEATURE0" -> 1,
    "FEATURE1" -> 2
  )*/
  //Build feature map dynamically right after entry point in the phase (GreedyAndBackfit_P1)
  var fieldIndexMap : Map[String, Integer] = Map[String, Integer]()


  /************************************ Main ****************************/

  def main(args: Array[String]): Unit = {
    val spark = SparkSession
      .builder()
      .appName("SparkSessionZipsExample")
      .getOrCreate()

    import spark.implicits._

    val df = spark.read
      .format("com.databricks.spark.csv")
      .option("header", "true")
      .option("inferSchema",  "true")
      .csv("file:///home/dan/IdeaProjects/Backfitting/data/backfitting.csv")

    df.show()

    // Testing
    println("Testing begins: ")


    println("The minimaxAPR is: " + GreedyAndBackfit_P1(df, Array[String]("FEATURE0", "FEATURE1"), spark))

  }

  /************************************ Phase 1 Merged ****************************/

  /**
    * Perfroms the first step of the APR algorithm, i.e. Greedy Selection + Backfitting
    *
    * @param df the DataFrame of positive bags
    * @param relevantFeatures the set of relevant features
    * @param spark the SparkSession
    * @return an APR
    */
  def GreedyAndBackfit_P1(df : DataFrame,
                          relevantFeatures : Array[String],
                          spark : SparkSession) : Map[String, (Double, Double)] = {

    // Declare the relevant variables for this Algorithm stage

    //add INCR column
    val dff = df.withColumn("INCR",lit(Double.MaxValue))

    /*dff.printSchema()
    dff.show()*/

    //Build fieldIndexMap
    fieldIndexMap = Map[String, Integer](
      dff.schema.fieldNames.zipWithIndex.map{ case (feature: String, index: Int) => feature -> new Integer(index) }.toSeq:_*
    )

    /*println("MAP--------------")
    fieldIndexMap.foreach(println(_))*/


    //System.exit(1)

    // the decisions vector
    var decisions = Array[Row]()

    // Save to var, so as to allow change
    var greedyDF = dff



    // the backfitting DF
    var backfitDF = spark.createDataFrame(spark.sparkContext.emptyRDD[Row], greedyDF.schema)

    // compute the initial APR, i.e. the minimaxAPR (more specifically, every feature's bounds
    // will be equalized using the formula newBound = (ub_d + lb_d) / 2
    var APR : Map[String, (Double, Double)] = minimaxAPR(greedyDF, relevantFeatures)

    // Need to have at least two decisions before starting the algorithm --> one step before the while
    // APR will be changed to the equal bound APR of the first decision's values
    val computeNextResult = computeNext(APR, relevantFeatures, greedyDF, decisions, backfitDF)

    decisions = computeNextResult._1
    backfitDF = computeNextResult._2
    greedyDF = computeNextResult._3

    APR = createAPRFromSingleDecision(decisions(0), relevantFeatures)



    //CHANGED so count is only done once
    val positiveBags = greedyDF.dropDuplicates("BAG_ID").count

    // Perform loop until no bags remain for greedy extraction
    for (i <- 1.toLong to positiveBags) {
      val computeNextResult = computeNext(APR, relevantFeatures, greedyDF, decisions, backfitDF)

      decisions = computeNextResult._1
      backfitDF = computeNextResult._2
      greedyDF = computeNextResult._3



      /*backfitDF = backfitDF.cache()
      greedyDF = greedyDF.cache()*/

      val sp = backfitDF.sparkSession
      backfitDF= backfitDF.sqlContext.createDataFrame(sp.sparkContext.parallelize(backfitDF.collect().toSeq), backfitDF.schema)

      greedyDF= greedyDF.sqlContext.createDataFrame(sp.sparkContext.parallelize(greedyDF.collect().toSeq), greedyDF.schema)


      //decisions.foreach(println(_))


      val backfittingResult = backfitting(decisions, backfitDF, decisions.length - 1, relevantFeatures)

      decisions = backfittingResult._1
      APR = backfittingResult._2
    }


    printDecisions(decisions)

    // Return the APR
    APR
  }

  /**
    * Creates an APR from a single decision
    *
    * @param decision the decision which will be used to create the APR
    * @param relevantFeatures the set of relevant features
    * @return a single decision APR
    */
  def createAPRFromSingleDecision(decision : Row,
                                  relevantFeatures : Array[String]) : Map[String, (Double, Double)] = {

    var apr = Map[String, (Double, Double)]()

    //println("Decision: ")
    //println(decision)

    //println("Array of features: ")
    //relevantFeatures.foreach(println)

    // Create APR with equal bounds (they are equal to the value of the feature in the decision)
    for (feature <- relevantFeatures)
    {
      //println("Feature: " + feature)

      val feat = decision.getDouble(fieldIndexMap(feature))

      apr += (feature -> (feat, feat))
    }

    // Return the APR
    apr
  }

  /************************************ MinimaxAPR ****************************/

  /**
    * Generate the minimaxAPR
    *
    * @param df the DataFrame
    * @param relevantFeatures the names of the relevant features
    * @return the minimaxAPR
    */
  def minimaxAPR(df : DataFrame,
                 relevantFeatures : Array[String]) : Map[String, (Double, Double)] = {

    // Change DF to var, so as to allow modification
    //var df = dff

    /*
    // UPDATE: THIS IS NO LONGER NEEDED, AS IT BASICALLY BUILDS AN ARRAY OF THE RELEVANT COLUMNS -> REDUNDANT
    // Get the numeric columns
    for (col <- df.schema.fields) {

      // If the column is both relevant and numeric it will be taken into account
      // Update: ALL INSTANCES WILL BE NUMERIC WHEN THEY GET HERE, NO NEED FOR NUMERIC CHECK, OR CAST
      if (relevantFeatures.contains(col.name) /* && col.dataType.isInstanceOf[NumericType] */) {
        // df = df.withColumn(col.name, df.col(col.name).cast(DoubleType))
        goodColumns = goodColumns :+ col.name
      } else
        badColumns = badColumns :+ col.name
    }
    */

    /*
    goodColumns.foreach(println)
    println("--------------------------------------------------------------------------------")
    badColumns.foreach(println)
    */

    // goodColumns = relevantFeatures
    //goodColumns = goodColumns.filter(!_.equals("BAG_ID"))

    val dfMax  = df.groupBy("BAG_ID").max(relevantFeatures : _*)
    val dfMin  = df.groupBy("BAG_ID").min(relevantFeatures : _*)

    val upperBound = dfMin.groupBy().max(dfMin.schema.fieldNames.filter(!_.equals("BAG_ID")) : _*)
    val lowerBound = dfMax.groupBy().min(dfMax.schema.fieldNames.filter(!_.equals("BAG_ID")) : _*)


    var minimaxAPR = Map[String, (Double, Double)]()

    for (i <- relevantFeatures.indices) {
      /*
      // Perform swap if bounds are reversed, e.g. (11.0, 7.0)
      var result = Tuple2[Double, Double](lowerBound.first().getAs(i), upperBound.first().getAs(i))

      if (result._1 > result._2)
        result = result.swap
      */
      val division = (lowerBound.first().getAs[Double](i) + upperBound.first().getAs[Double](i)) / 2

      minimaxAPR += (relevantFeatures(i) -> (division, division))
    }

    // Return the minimaxAPR
    minimaxAPR
  }


  /************************************ Razvan ****************************/


  def computeIncr(featureValues: Seq[Double],
                  featureNames: Array[String],
                  relevantFeatures: Array[String],
                  APR: Map[String, (Double, Double)]): Double = {
    var incrT = 0.0

    for (relF <- relevantFeatures) {
      var incr = 0.0

      val APRBoundRelF = APR.get(relF).head
      val instValRelF = featureValues(featureNames.indexOf(relF))

      if (instValRelF < APRBoundRelF._1)
        incr = APRBoundRelF._1 - instValRelF
      else if (instValRelF > APRBoundRelF._2)
        incr = instValRelF - APRBoundRelF._2

      incrT += incr
    }

    incrT
  }


  def computePerInstanceIncrease(relevantFeatures: Array[String],
                                 APR: Map[String, (Double, Double)],
                                 df: DataFrame) : DataFrame = {
    var columns = Array[Column]()
    columns = df.columns.filter(!_.equals("BAG_ID")).map(df(_))

    val arrayColumns = array(columns: _*)

    val featureNames = df.schema.fieldNames.filter(!_.equals("BAG_ID"))

    val modUDF = udf((x: Seq[Double]) => computeIncr(x, featureNames, relevantFeatures, APR))
    //val modUDF = udf((x:Seq[Double])=>computeIncrN(x, APR))

    df.withColumn("INCR", modUDF(arrayColumns))
  }

  def replaceNumericTypesWithDouble(df: DataFrame) : DataFrame = {
    var dfR = df

    for (c <- df.schema.fields)
      if (c.dataType.isInstanceOf[NumericType]) {

        dfR = dfR.withColumn(c.name, dfR(c.name).cast(DoubleType))
      }

    dfR
  }


  def getBestBagInstance(df: DataFrame): (DataFrame, Row) = {
    /*val dfMins = df.groupByKey(x=>x.getAs[Int]("BAG_ID")).mapGroups((bagID, rows) => Seq(bagID, rows.maxBy(_.getAs[Double]("INCR")).toSeq)).toDF(df.schema.fieldNames:_*)




    val minCol = dfMins.schema.fieldNames.filter(!_.equals("BAG_ID"))*/

    val minDF = df.orderBy("INCR")

    val minRow = minDF.first()
    //val minIncr = minRow.getAs[Double]("INCR")
    val minIncrBag = minRow.getAs[Int]("BAG_ID")



    val bestBag = df.filter(df("BAG_ID").equalTo(minIncrBag))

    //val bestInst = bestBag.filter(df("INCR").equalTo(minIncr)).first()

    (bestBag, minRow)

  }

  def computeNext(APR: Map[String, (Double, Double)],
                  relFeatures: Array[String],
                  compNextDF: DataFrame,
                  decArray: Array[Row],
                  backfitDF: DataFrame) : (Array[Row], DataFrame, DataFrame) = {

    val compIncrDF = computePerInstanceIncrease(relFeatures, APR, compNextDF)

    var (bestBag, bestInstance) = getBestBagInstance(compIncrDF)

    //bestBag.show()
    //println(bestInstance.toString())

    //bestBag.printSchema()
    //backfitDF.printSchema()

    // Check the selected best instance
    //println("The best instance: " + bestInstance)

    // Remove the new column so as to allow union and difference
    /*bestBag = bestBag.drop("INCR")
    compIncrDF = compIncrDF.drop("INCR")*/

    /*
    compIncrDF.show()
    bestBag.show()
    backfitDF.show()
    */

    val sp = bestBag.sparkSession

    import sp.implicits._

    bestBag = bestBag.sqlContext.createDataFrame(sp.sparkContext.parallelize(bestBag.collect().toSeq), bestBag.schema)

    //bestBag.cache()

    (decArray :+ bestInstance, backfitDF.union(bestBag), compIncrDF.except(bestBag))
  }

  /************************************ Dan ******************************/

  /**
    * Generate an APR from the decisions taken so far, and the relevant features
    *
    * @param decisions the decision vector
    * @param relevantFeatures the relevant features vector
    * @return An APR created relative to the decisions and relevantFeatures
    */
  def createAPR(decisions : Array[Row],
                relevantFeatures : Array[String]) : Map[String, (Double, Double)] = {
    var apr : Map[String, (Double, Double)] =  Map[String, (Double, Double)]()

    // Create the initial APR
    for (feature <- relevantFeatures)
      apr += (feature -> (Double.MaxValue, Double.MinValue))

    // Iterate through each decision,
    for (decision <- decisions)
      for (dimension <- apr) {
        var min = dimension._2._1
        var max = dimension._2._2

        //println("The decision is: " + decision.toString())

        /*
        println("The feature is: " + dimension._1)
        println("The index is: " + fieldIndexMap(dimension._1))

        println("The decision is: " + decision.toString())
        */

        // Get the value of the current feature from the row
        val currentFeatureRowVal = decision.getDouble(fieldIndexMap(dimension._1))

        if (currentFeatureRowVal < min)
          min = currentFeatureRowVal

        if (currentFeatureRowVal > max)
          max = currentFeatureRowVal

        // If the APR needs to be updated, then update the APR
        if (!dimension._2.equals((min, max))) {
          apr = apr + (dimension._1 -> (min, max))
          //apr = apr.updated(dimension._1, (min, max))
          //apr.put(dimension._1, Tuple2[Double, Double](min, max))
        }
      }

    // Return the apr
    apr
  }

  /**
    * Returns the best instance from a particular bag, passed as parameter
    *
    * @param apr the current apr, relative to which the cost will be calculated
    * @param relevantFeatures the relevant features vector
    * @param dff the DataFrame containing the bag
    * @return the best instance of the bag, which reduces the APR least, i.e. least cost instance from bag
    */
  def getBestInstanceForBag(apr : Map[String, (Double, Double)],
                            relevantFeatures : Array[String],
                            dff : DataFrame,
                            bagId : Integer) : Row = {

    //val df = dff.drop("BAG_ID")

    //var minIncrease : Double = Double.MaxValue
    //var minRow : Row = Row()

    //val featureNames = relevantFeatures

    // Define inner method for selecting the row with the minimal increase
    /*def rowFunction(x : Row) : Unit = {
      //println("The row is: " + x)

      val increase = computeIncr(x.toSeq.asInstanceOf[Seq[Double]], featureNames, relevantFeatures, apr)

      if (increase < minIncrease) {
        minIncrease = increase
        minRow = x
      }
    }*/

    /*
      // Apply the function on each row
      //df.collect().foreach(rowFunction)
      val (_, minRow) = getBestBagInstance(dff)


      //println("The cost is: " + minIncrease)

      // Return the row
      // Prepend the bagId to the row
      Row.fromSeq(minRow.toSeq)
  */

    // Return the least cost decision
    /*computePerInstanceIncrease(relevantFeatures, apr, dff)
      .reduce((x, y) => {
        if (x.getAs[Double]("INCR") > y.getAs[Double]("INCR"))
          y
        else
          x
      })*/


    computePerInstanceIncrease(relevantFeatures, apr, dff)
      .orderBy("INCR")
      .first()
  }

  def printDecisions(decisions : Array[Row]): Unit = {

    println("Start Print")

    val iter :Iterator[Row] = decisions.iterator

    while(iter.hasNext)
      println("Decision: " + iter.next())


    println("End Print")

  }

  /**
    * Will perform Backfitting
    *
    * @param decisionsPar the decisions taken so far (in terms of chosen Rows/Instances)
    * @param df the DataFrame containing the bags of the instances in decisionsPar
    * @param n the upper limit of the decision, i.e. checking will be performed from 0 -> n (inclusive)
    * @param relevantFeatures the relevant features relative to which the APR will be calculated
    * @return tuple(newDecisions, APR)
    */
  def backfitting(decisionsPar : Array[Row],
                  df : DataFrame,
                  n : Integer,
                  relevantFeatures : Array[String]) : (Array[Row], Map[String, (Double, Double)]) = {

    // Will control the reiterations
    var redo : Boolean = false

    // Assign to var so as to allow modification
    var decisions = decisionsPar


    do {
      // Reset redo flag
      redo = false

      // from 0 up to latestDecision
      for (i <- 0 to n) {

        // Get the row currently under evaluation
        val current: Row = decisions(i)

        //println("The current is: " + current)
        //printDecisions(decisions)
        // Discard current decision from the decisions vector
        decisions = decisions.take(i) ++ decisions.drop(i + 1)

        //printDecisions(decisions)

        // create the APR from the decision vector
        // Array[String]() will need to be replaced by a set of the names of the relevant features
        val apr: Map[String, (Double, Double)] = createAPR(decisions, relevantFeatures)

        val bagId = current.getInt(fieldIndexMap.get("BAG_ID").head)

        // Get the best instance of the current bag
        val bestInstance: Row = getBestInstanceForBag(apr,
          relevantFeatures,
          df.where(df.col("BAG_ID").equalTo(bagId)),
          bagId)

        // If the currentBest instance is different from the current instance -> change decision + recursive call
        decisions = decisions.take(i) ++ Array(bestInstance) ++ decisions.drop(i)

        // the backfitting part refers to recursively performing the adjustment
        if (!bestInstance.equals(current))
        //backfitting(decisionsPar, df, i)
          redo = true
      }

      //decisions.foreach(println)
      //println("Going again")

    } while(redo)


    // Return the tuple
    (decisions, createAPR(decisions, relevantFeatures))
  }

}