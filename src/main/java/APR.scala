package main.java

import java.io.{File, PrintWriter}

import org.apache.log4j.{Level, Logger}
import org.apache.spark.ml.feature.StringIndexer
import org.apache.spark.sql.types.{DoubleType, IntegerType, LongType}
import org.apache.spark.sql.{DataFrame, SparkSession}

import scala.io.Source

/**
  * Created by dan on 15.03.2017.
  */
object APR {

  def main(args : Array[String]) : Unit = {

    if (args.length != 5) {

      println("Too few Arguments!\n" +
        "Usage: SequentialAPR-1.0-SNAPSHOT-jar-with-dependencies.jar " +
        "POSITIVE_CSV_NAME NEGATIVE_CSV_NAME OUTPUT_FILE BAG_ID_COLUMN_NAME PATH_TO_REL_FEATURES <RETURN>")

      System.exit(0xFF)
    }

    // Read the path from the command line
    //val path = args(0)

    val spark = SparkSession
      .builder()
      .appName("SparkSessionZipsExample")
      .getOrCreate()

    val rootLogger = Logger.getRootLogger()
    rootLogger.setLevel(Level.ERROR)

    // The two DataFrames must have the same headers!
    val dffPositives = spark.read
      .format("com.databricks.spark.csv")
      .option("header", "true")
      .option("inferSchema",  "true")
      .csv(args(0))

    // The two DataFrames must have the same headers!
    val dffNegatives = spark.read
      .format("com.databricks.spark.csv")
      .option("header", "true")
      .option("inferSchema",  "true")
      .csv(args(1))

    // Read the relevant features from a file

    val relevantFeatures : Array[String] = parseRelevantFeaturesFile(args(4))

    val apr = GenerateAPRClassifier(dffPositives,
                                    dffNegatives,
                                    spark,
                                    args(3),
                                    relevantFeatures)

    // Display the APR
    displayAPR(apr)

    // Write the APR
    writeAPRToFile(apr, args(2))


  }

  /**
    * USE THIS: Generate an APR Classifier relative to the positive and negative DF, and the excluded features.
    * The positiveDF and the negativeDF must both have the same headers.
    *
    * @param positiveDF the positive bags DataFrame
    * @param negativeDF the negative bags DataFrame
    * @param spark the SparkSession
    * @param bagIdColumnName the column name where the bag id is hosted
    * @param relevantFeatures the initial relevant features
    * @return an APR classifier
    */
  def GenerateAPRClassifier(positiveDF : DataFrame,
                            negativeDF : DataFrame,
                            spark : SparkSession,
                            bagIdColumnName : String,
                            relevantFeatures : Array[String]) : Map[String, (Double, Double)] = {

    // Format the DataFrames so as to make them compatible with the APR Algorithm
    val dfPositives = transformData(positiveDF, bagIdColumnName, relevantFeatures)
    val dfNegatives = transformData(negativeDF, bagIdColumnName, relevantFeatures)

    dfPositives.printSchema()
    dfPositives.show()

    dfNegatives.printSchema()
    dfNegatives.show()

    //System.exit(10)

    // Prepare the feature vectors which control the do-while loop
    var oldRelevantFeatures = relevantFeatures
    var newRelevantFeatures = relevantFeatures

    // Declare the APR object
    var APR : Map[String, (Double, Double)] = Map[String, (Double, Double)]()

    // Declare the Phase Objects
    // Phase 1 - Greedy and Backfit (Growth stage)
    val greedyAndBackfit = Phase1_GreedyAndBackfit

    // Phase 2 - Feature Selection (Discrimination stage)
    val featureSelection = FeatureSelection

    // Phase 3 - Margin Expand (Expand stage)
    val marginExpand = MarginExpand

    // Start the algorithm
    // Phase 1 and 2 Loop until relevant features converge
    do {
      // Get the most relevant features
      oldRelevantFeatures = newRelevantFeatures

      // Generate an Initial APR based on the positive bags
      APR = greedyAndBackfit.GreedyAndBackfit_P1(dfPositives, oldRelevantFeatures, spark)

      /*
      for (dimensions <- APR)
        println("Feature " + dimensions._1 + " bounds: [" + dimensions._2._1 + ", " + dimensions._2._2 + "]")
      */

      // Get the relevant features based on the negative bags
      newRelevantFeatures = featureSelection.featureSelection(dfNegatives, APR, oldRelevantFeatures, 0.05)

    } while(!oldRelevantFeatures.sameElements(newRelevantFeatures))

    // Phase 3 - Expand the APR bounds
    APR = marginExpand.expandMargins(APR, dfPositives)

    //APR is done -> return it
    APR
  }

  /**
    * DO NOT USE THIS: Generate an APR Classifier relative to the positive and negative DF, and the excluded features.
    * The positiveDF and the negativeDF must both have the same headers.
    *
    * @param positiveDF the positive bags DataFrame
    * @param negativeDF the negative bags DataFrame
    * @param spark the SparkSession
    * @param excludedFeatures the excluded features
    * @return an APR classifier
    */
  def GenerateAPRClassifier(positiveDF : DataFrame,
                            negativeDF : DataFrame,
                            spark : SparkSession,
                            excludedFeatures : Array[String] = Array[String]()) : Map[String, (Double, Double)] = {

    // 1st variant
    // Prepare the Data - Positive Bags
    val transformedDataPositives = transformData(positiveDF)

    // Declare the DataFrame and relevant Features variables
    val dfPositives = transformedDataPositives._1
    var oldRelevantFeatures = Array[String]()
    var newRelevantFeatures = transformedDataPositives._2

    // Prepare the Data - Negative Bags
    val dfNegatives = transformData(negativeDF)._1

    // END

    // Declare the APR object
    var APR : Map[String, (Double, Double)] = Map[String, (Double, Double)]()

    // Declare the Phase Objects
    // Phase 1 - Greedy and Backfit (Growth stage)
    val greedyAndBackfit = Phase1_GreedyAndBackfit

    // Phase 2 - Feature Selection (Discrimination stage)
    val featureSelection = FeatureSelection

    // Phase 3 - Margin Expand (Expand stage)
    val marginExpand = MarginExpand

    // Start the algorithm
    // Phase 1 and 2 Loop until relevant features converge
    do {
      // Get the most relevant features
      oldRelevantFeatures = newRelevantFeatures

      // Generate an Initial APR based on the positive bags
      APR = greedyAndBackfit.GreedyAndBackfit_P1(dfPositives, oldRelevantFeatures, spark)

      // Get the relevant features based on the negative bags
      newRelevantFeatures = featureSelection.featureSelection(dfNegatives, APR, oldRelevantFeatures, 0.05)

    } while(!oldRelevantFeatures.sameElements(newRelevantFeatures))

    // Phase 3 - Expand the APR bounds
    APR = marginExpand.expandMargins(APR, dfPositives)

    //APR is done -> return it
    APR
  }

  /**
    * USE THIS: Transforms a DF passed as parameter to something that suits the APR algorithm
    *
    * @param initialDf the DF which will be transformed
    * @param bagIdColumn the name of the column hosting the BagId
    * @param relevantColumns the relevant features which will be kept and transformed (if need be)
    * @return
    */
  def transformData(initialDf : DataFrame,
                    bagIdColumn : String,
                    relevantColumns : Array[String]) : DataFrame = {

    // Cast to var so as to allow manipulation
    var df = initialDf

    val exceptionColumns = bagIdColumn +: relevantColumns

    // Set up the pipeline
    //var pipeline = new Pipeline()
    //var stages = Array[PipelineStage]()


    // Eliminate the irrelevant columns
    for (columnName <- df.schema.fields
         if !exceptionColumns.contains(columnName.name))
      df = df.drop(columnName.name)

    for (column <- df.schema.fields) {

      // If column type is Integer or Long -> cast is as Double
      if ((column.dataType.isInstanceOf[IntegerType] || column.dataType.isInstanceOf[LongType])  &&
        !column.name.equals(bagIdColumn))
        df = df.withColumn(column.name, df.col(column.name).cast(DoubleType))

      // If the column is StringType -> Index it
      if (column.dataType.isInstanceOf[org.apache.spark.sql.types.StringType]) {
        val indexer = new StringIndexer()
          .setInputCol(column.name)
          .setOutputCol(column.name + "Indexed")

        // Save the new indexed DataFrame
        // Might affect the for loop due to the schema change
        df = indexer
          .fit(df)
          .transform(df)
          .drop(column.name)
      }

    }

    // Return the transformed df
    df
  }

  /**
    * DO NOT USE THIS: Adapts a DataFrame to the APR algorithm
    *
    * @param initialDf the initial DataFrame which need to be adapted to the APR algorithm
    * @param eliminatedColumns specifies which columns should be discarded from the very beginning; by default empty
    * @return a tuple of the new, transformed DataFrame, and a feature vector
    */
  def transformData(initialDf : DataFrame,
                    eliminatedColumns : Array[String] = Array()) : (DataFrame, Array[String]) = {

    // Cast to var so as to allow manipulation
    var df = initialDf
    var relevantFeatures = Array[String]()

    // Set up the pipeline
    //var pipeline = new Pipeline()
    //var stages = Array[PipelineStage]()

    // Eliminate the unnecessary columns
    for (columnName <- eliminatedColumns)
      df = df.drop(columnName)

    for (column <- df.schema.fields) {

      // If column type is Integer or Long -> cast is as Double
      if (column.dataType.isInstanceOf[IntegerType] || column.dataType.isInstanceOf[LongType])
        df = df.withColumn(column.name, df.col(column.name).cast(DoubleType))

      // If the column is StringType -> Index it
      if (column.dataType.isInstanceOf[org.apache.spark.sql.types.StringType]) {
        val indexer = new StringIndexer()
                          .setInputCol(column.name)
                          .setOutputCol(column.name + "Indexed")

        // Save the new indexed DataFrame
        // Might affect the for loop due to the schema change
        df = indexer
              .fit(df)
              .transform(df)
              .drop(column.name)
      }

    }

    for (column <- df.schema)
      relevantFeatures = relevantFeatures :+ column.name

    // Return the transformed df, and the relevant features
    (df, relevantFeatures)
  }


  /**
    * Prints the APR passed as parameter
    *
    * @param apr the APR which will be printed
    */
  def displayAPR(apr : Map[String, (Double, Double)]) : Unit = {

    apr.foreach(x =>{

      println("The bounds of feature " + x._1 + " are: [" + x._2._1 + ", " + x._2._2 + "]")

    })

  }

  /**
    * Writes an APR to a file specified via a path
    *
    * @param apr the APR to be writter
    * @param path the path where the file is located/will be created
    */
  def writeAPRToFile(apr : Map[String, (Double, Double)],
                     path: String) : Unit = {

    val writer = new PrintWriter(new File(path))

    writer.println("NAME,LOWER,UPPER")

    apr.foreach(x =>{

      writer.println(x._1 + "," + x._2._1 + "," + x._2._2)

    })

    writer.close()
  }

  /**
    * Reads the relevant features as specified in a file
    *
    * @param path the path to the file specifying the relevant features
    * @return an array containing the relevant features as specified in the file
    */
  def parseRelevantFeaturesFile(path : String) : Array[String] = {

    var relevantFeatures : Array[String] = Array[String]()

    for(line <- Source.fromFile(path).getLines())
      relevantFeatures = relevantFeatures :+ line

    // Return the relevant features
    relevantFeatures
  }

}
