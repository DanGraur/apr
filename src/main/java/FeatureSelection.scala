package main.java

import java.io.{File, FileOutputStream, PrintWriter}
import javax.imageio.stream.FileImageOutputStream

import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.{Column, DataFrame, Dataset, Row, SparkSession}
import org.apache.spark.sql.functions.{array, lit, udf}
import org.apache.spark.sql.types.{DoubleType, NumericType}

/**
  * Created by dan on 11.03.2017.
  */
object FeatureSelection {

  /************************************ Main ****************************/

  def main(args : Array[String]) : Unit = {
    val spark = SparkSession
      .builder()
      .appName("SparkSessionZipsExample")
      .getOrCreate()

    val rootLogger = Logger.getRootLogger()
    rootLogger.setLevel(Level.FATAL)

    import spark.implicits._

    val df = spark.read
      .format("com.databricks.spark.csv")
      .option("header", "true")
      .option("inferSchema",  "true")
      .csv("file:///home/dan/IdeaProjects/Backfitting/data/BACKUP/ArtificialSet_2/Split/artificialDataPositives.csv")

    df.show()

    println("Testing begins: ")

    val apr : Map[String, (Double, Double)] = Map[String, (Double, Double)](
      "FEATURE0" -> (47.1, 52.3),
      "FEATURE1" -> (40.9, 44.5)
    )

    /*
    val dff = discriminatoryFeaturesPerInstance(df, Array[String]("FEATURE0", "FEATURE1"), apr)
    val map = filterDiscriminatedInstances(dff, Array[String]("FEATURE0", "FEATURE1"), apr, 0.5)

    println("Printing the initial DF:")
    dff.show()

    println("Printing the maps:")
    for (x <- map)
      x._2.show()
    */


    featureSelection(df, apr, Array[String]("FEATURE0", "FEATURE1"), 0.5)
  }

  /************************************ Phase 2 Merged ****************************/

  /**
    * Perform feature selection
    *
    * @param dff the DataFrame containing the 'negative' instances (in our case the instances that have no errors)
    * @param apr the APR, as obtained from the previous phase
    * @param oldRelevantFeatures the previously relevant features
    * @param limit the limit above which a feature will strongly discriminate by default
    * @return a relevant feature vector
    */
  def featureSelection(dff : DataFrame,
                       apr: Map[String, (Double, Double)],
                       oldRelevantFeatures : Array[String],
                       limit : Double) : Array[String] = {

    // Save to var so as to change it later, and filter out those which are within the current APR
    var df = dff.filter(x => {
      var isOut = false

      // Check if the current instance fits within the bounds of the APR
      for (feature <- apr if !isOut) {

        // Get the feature value
        val value : Double = x.getAs[Double](feature._1)

        // Check the bounds
        if (value < feature._2._1 || value > feature._2._2)
          isOut = true
      }

      // Return the answer
      isOut
    })

    // Store the old Relevant Features in a var so as to allow them to be removed
    var relevantFeatures = oldRelevantFeatures

    // Vector of new Relevant Features
    var newRelevantFeatures : Array[String] = Array[String]()

    // Get the augmented DF, with maximal out of bounds feature - Compute only once; remove instances from it in the loop
    var maxOutOfBoundsDF = discriminatoryFeaturesPerInstance(df, relevantFeatures, apr)

    // Keep iterating until all 'negative' instances have been removed,
    // or the relevant features have been all consumed (this will only be the case when there is a negative entry within the APR bounds)
    // alternatively a filter could be applied to the DF to remove these instances beforehand
    while(relevantFeatures.nonEmpty && df.count() > 0) {

      // Get a map of the String -> Discriminated Instances (as DF)
      val mapOfDiscriminations = filterDiscriminatedInstances(maxOutOfBoundsDF, relevantFeatures, apr, limit)

      // Start the process of removing the most discriminating feature
      var maxCount : Long = 0L
      var featureName = ""

      // Find most discriminating feature
      for (entry <- mapOfDiscriminations) {
        val entryCount = entry._2.count()

        if (entryCount > maxCount) {
          maxCount = entryCount
          featureName = entry._1
        }
      }

      // Remove its instances from the DF
      df = df.except(mapOfDiscriminations(featureName).drop("BEST_BOUND"))
      // And from the maxOutOfBoundsDF
      maxOutOfBoundsDF = maxOutOfBoundsDF.except(mapOfDiscriminations(featureName))

      // Remove the feature from the old relevant features, so as to avoid it being retested
      relevantFeatures = relevantFeatures.filter(!_.equals(featureName))

      // Add the extracted feature to the relevant features
      newRelevantFeatures = newRelevantFeatures :+ featureName

    }

    // Return the relevant features
    newRelevantFeatures
  }

  /************************************ Razvan ****************************/

  /**
    * Generates a Map[String, DataFrame], i.e. mapping from feature to instances that feature discriminates against
    *
    * @param df the DataFrame of instances to be discriminated against
    * @param relFeatures the current relevant features, which will be tested for future relevance
    * @param APR the APR which will be used to test the 'negative' instances (in our case it will be the component instances that have no error)
    * @param limit the fraction beyond the bound which will make a feature discriminatory
    * @return a map data structure, mapping between feature names, and the instances which that feature discriminates
    */
  def filterDiscriminatedInstances(df: DataFrame,
                                   relFeatures: Array[String],
                                   APR: Map[String, (Double, Double)],
                                   limit: Double) : Map[String, DataFrame] = {

    // Maps between the feature name, and the instances they discriminate against
    var map = Map[String, DataFrame]()

    // featureVal - the value of the feature being tested
    // testedFeature - the name of the feature being tested
    // bestBoundDifferenceR - column in the instance which stores a tuple (will be cast as a row)
    val filterFunc = (featureVal: Double, testedFeature: String, bestBoundDifferenceR: Row) => {

      // Break the row in a tuple (BestFeatureName, Percentage)
      val bestBoundDifference: Tuple2[String, Double] = (bestBoundDifferenceR.getAs[String](0), bestBoundDifferenceR.getAs[Double](1))

      // Will store the bound difference
      var boundDifference = 0.0

      // Get the bounds of the feature from the APR
      val bound = APR.get(testedFeature).head

      // Compute the difference against the bound
      if (featureVal < bound._1)
        boundDifference = bound._1 - featureVal
      else if (featureVal > bound._2)
        boundDifference = featureVal - bound._2

      // Needs percentage computation
      val percentage : Double = getPercentageOutOfBounds(boundDifference, bound._1, bound._2)

      // Return true of false, based on the below boolean expression
      percentage >= limit || (bestBoundDifference._2 < limit && bestBoundDifference._1.equals(testedFeature))
    }

    // Iterate through each feature
    for (relFeature <- relFeatures) {

      // Define a wrapper function which will call filterFunc
      val udfFilterFunc = udf((featureVal: Double, bestBoundDifference: Row) => filterFunc(featureVal, relFeature, bestBoundDifference))

      // Apply the wrapper function on the df iteratively, in order to obtain the filtered DF for each of the features
      map += (relFeature -> df.filter(udfFilterFunc(df(relFeature), df("BEST_BOUND"))))
    }

    // Return the Map
    map
  }


  /************************************ Dan ****************************/

  /**
    * Computes the Out of Bounds Percentage for a feature
    *
    * @param outOfBoundsValue the out of bounds value of the feature
    * @param lowerBound the lower bound of the feature (as specified by the APR)
    * @param upperBound the upper bound of the feature (as specified by the APR)
    * @return the Out of Bounds Percentage
    */
  def getPercentageOutOfBounds(outOfBoundsValue : Double,
                               lowerBound : Double,
                               upperBound : Double) : Double = {

    val range = Math.abs(upperBound - lowerBound)

    //println("Range is: " + range + " out of BoundValue")

    // return the Out of Bounds Percentage
    outOfBoundsValue / range
  }


  /**
    * Add an additional column to the DataFrame, containing the most discriminatory feature for that particular instance
    *
    * @param df the original DataFrame
    * @param relevantFeatures the names of the relevant features, which will be tested
    * @return a new DataFrame, containing an extra column for each instance, where both the name of the most discriminatory
    *         feature will be stored and the percentage by which the feature value is outside the bounds
    */
  def discriminatoryFeaturesPerInstance(df : DataFrame,
                                        relevantFeatures : Array[String],
                                        apr : Map[String, (Double, Double)]) : DataFrame = {

    // Get the relevant columns, i.e. the columns representing the relevant features
    val relevantColumns : Array[Column] = df.columns.filter(relevantFeatures.contains(_)).map(df(_))

    val columnArray = array(relevantColumns : _*)

    // Declare an array which will store the column names in the same order as they are passed to the UDF
    // This will avoid possible issues with ordering discrepancy between the relevantFeatures order
    // and the schema.fieldNames order
    val relevantColumnNames : Array[String] = df.schema.fieldNames.filter(relevantFeatures.contains(_))

    //println("Printing the columns: ")
    //relevantColumnNames.foreach(println)

    val maxFeatureColumnUdf = udf(
      // columns will represent the values of the relevant features for each row
      (columns : Seq[Double]) => {

        var maxFeature : String = ""
        var maxPercentage : Double = 0.0

        for (feature <- relevantFeatures) {
          // Get the bounds of this feature
          val bounds : (Double, Double) = apr(feature)

          // Get the out of bounds value
          val featureVal = columns(relevantColumnNames.indexOf(feature))
          var boundDifference = 0.0

          if (featureVal < bounds._1)
            boundDifference = bounds._1 - featureVal
          else if (featureVal > bounds._2)
            boundDifference = featureVal - bounds._2

          // get the feature Out of Bounds Percentage for this feature
          val featurePercentage = getPercentageOutOfBounds(boundDifference, bounds._1, bounds._2)

          //println("The bound is: " + bounds + " the percentage is " + featurePercentage)

          // If the percentage is higher, update the maxPercentage and the maxFeature
          if (maxPercentage < featurePercentage) {
            maxPercentage = featurePercentage
            maxFeature = feature
          }
        }

        // Return the tuple
        //Row.fromSeq(Seq[Any](maxFeature, maxPercentage))
        (maxFeature, maxPercentage)
      }
    )

    // Return the DataFrame with the new added column
    df.withColumn("BEST_BOUND", maxFeatureColumnUdf(columnArray))
  }


}
